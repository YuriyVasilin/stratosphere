<?php


namespace App\Controller;

use App\Form\EditUserType;
use App\Form\RegistrationFormType;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class MainPageController extends AbstractController
{
    /**
     * @Route("/", name="main_page")
     * @IsGranted("ROLE_USER")
     */
    public function new(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $repository = $this->getDoctrine()->getRepository(User::class);
        $users = $repository->findAll();
        if (!$users) {
            throw $this->createNotFoundException(
                'List of users is empty'
            );
        }

        //Add new user
        $user = new User();
        $registration_form = $this->createForm(RegistrationFormType::class, $user);
        $registration_form->handleRequest($request);
        if ($registration_form->isSubmitted() && $registration_form->isValid()) {
            // encode the plain password
            $user->setRoles(['ROLE_USER']);
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $registration_form->get('plainPassword')->getData()
                )
            );

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('main_page');
        }

        //Edit old user
        $userOld = new User();
        $editUser_form = $this->createForm(EditUserType::class, $userOld);
        $editUser_form->handleRequest($request);
        if ($editUser_form->isSubmitted() && $editUser_form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $userNew = $entityManager->getRepository(User::class)->find($userOld->getId());
            $userNew->setFullName($userOld->getFullName());
            $userNew->setNickName($userOld->getNickname());
            $entityManager->persist($userNew);
            $entityManager->flush();

            return $this->redirectToRoute('main_page');
        }

        return $this->render('main_page/main_page.html.twig', [
            'users' => $users,
            'registrationForm' => $registration_form->createView(),
            'editUser' => $editUser_form->createView(),
        ]);
    }
}