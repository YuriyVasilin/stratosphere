<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EditUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        global $kernel;

        if ( 'AppCache' == get_class($kernel) )
        {
            $kernel = $kernel->getKernel();
        }

        $doctrine = $kernel->getContainer()->get( 'doctrine' );
        $repository = $doctrine->getRepository(User::class);
        $users = $repository->findAll();
        $users_id = [];
        foreach ($users as $user) {
            //array_push($users_id, $user->getId());
            $users_id[$user->getId()] = $user->getId();
        }
        $builder
            ->add('id', ChoiceType::class, [
                'choices'  => $users_id
            ])
            ->add('nickname')
            ->add('full_name')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
